' Name:         Hangman Game Project
' Purpose:      Simulates the Hangman game
' Programmer:   <Artur Wieczorek> on <28/II/2013>

Option Explicit On
Option Strict On
Option Infer Off

Public Class frmMain

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub

    Private Sub mnuFileNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileNew.Click

        Dim strWord As String
        Dim strLetter As String
        Dim blnValidWord As Boolean
        Dim blnDashReplaced As Boolean
        Dim blnGameOver As Boolean
        Dim intIncorrect As Integer

        ' hide the picture boxes
        picBottom.Visible = False
        picPost.Visible = False
        picTop.Visible = False
        picRope.Visible = False
        picHead.Visible = False
        picBody.Visible = False
        picRightArm.Visible = False
        picLeftArm.Visible = False
        picRightLeg.Visible = False
        picLeftLeg.Visible = False

        strWord = InputBox("Enter a 5-letter word:", "Hangman Game").ToUpper

        blnValidWord = True ' assume the word is valid

        If strWord.Length <> 5 Then
            blnValidWord = False
        Else
            Dim intIndex As Integer
            Do While intIndex < 5 AndAlso blnValidWord = True
                If strWord.Substring(intIndex, 1) Like "[!A-Z]" Then
                    blnValidWord = False
                End If
                intIndex = intIndex + 1
            Loop
        End If

        ' if the word is not valid, display a message
        If blnValidWord = False Then
            MessageBox.Show("5 letters are required.",
            "Hangman Game",
            MessageBoxButtons.OK,
            MessageBoxIcon.Information)
        Else

            ' display five dashes in lblWord and clear(lblIncorrect)
            lblWord.Text = "-----"
            lblIncorrect.Text = String.Empty

            ' get a letter from player 2 and convert to uppercase
            strLetter = InputBox("Enter a letter:", "Letter", "", 600, 400).ToUpper

            ' verify that player 2 entered a letter
            ' and that the game is not over
            Do While strLetter <> String.Empty AndAlso blnGameOver = False
                ' search the word for the letter
                For intIndex As Integer = 0 To 4
                    ' if the letter appears in the word, then
                    ' replace the dash in lblWord and
                    ' indicate that a replacement was made
                    If strWord.Substring(intIndex, 1) = strLetter Then
                        lblWord.Text =
                     lblWord.Text.Remove(intIndex, 1)
                        lblWord.Text =
                     lblWord.Text.Insert(intIndex, strLetter)
                        blnDashReplaced = True
                    End If
                Next intIndex

                ' determine whether a dash was replaced
                If blnDashReplaced = True Then
                    ' if the word does not contain any dashes,
                    ' the game is over because player 2
                    ' guessed the word; otherwise, reset the
                    ' blnDashReplaced variable for the next search
                    If lblWord.Text.Contains("-") = False Then
                        blnGameOver = True
                        MessageBox.Show("Great guessing!","Game Over",MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        blnDashReplaced = False
                    End If
                Else

                    ' processed when no dash was replaced()
                    ' display the incorrect letter, then update
                    ' the intIncorrect variable, then show
                    ' the appropriate picture box
                    lblIncorrect.Text = lblIncorrect.Text & " " & strLetter
                    intIncorrect = intIncorrect + 1
                    Select intIncorrect
                        Case 1
                            picBottom.Visible = True
                        Case 2
                            picPost.Visible = True

                        Case 3
                            picTop.Visible = True
                        Case 4
                            picRope.Visible = True
                        Case 5
                            picHead.Visible = True
                        Case 6
                            picBody.Visible = True
                        Case 7
                            picRightArm.Visible = True
                        Case 8
                            picLeftArm.Visible = True
                        Case 9
                            picRightLeg.Visible = True
                        Case 10
                            picLeftLeg.Visible = True
                            blnGameOver = True
                            MessageBox.Show("Sorry,the word Is " & strWord & ".", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Select
                End If

                ' determine whether to get another letter
                If blnGameOver = False Then
                    strLetter = InputBox("Enter a letter",
                    "Letter", "", 600, 400).ToUpper
                End If
            Loop
        End If
    End Sub
End Class


