<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblWord = New System.Windows.Forms.Label()
        Me.lblIncorrect = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.picLeftArm = New System.Windows.Forms.PictureBox()
        Me.picHead = New System.Windows.Forms.PictureBox()
        Me.picTop = New System.Windows.Forms.PictureBox()
        Me.picBottom = New System.Windows.Forms.PictureBox()
        Me.picRightArm = New System.Windows.Forms.PictureBox()
        Me.picLeftLeg = New System.Windows.Forms.PictureBox()
        Me.picRightLeg = New System.Windows.Forms.PictureBox()
        Me.picRope = New System.Windows.Forms.PictureBox()
        Me.picPost = New System.Windows.Forms.PictureBox()
        Me.picBody = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1.SuspendLayout()
        CType(Me.picLeftArm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picHead, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRightArm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLeftLeg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRightLeg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRope, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picPost, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBody, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Secret word:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(355, 31)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Incorrect guesses:"
        '
        'lblWord
        '
        Me.lblWord.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWord.Font = New System.Drawing.Font("Segoe UI", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWord.Location = New System.Drawing.Point(20, 50)
        Me.lblWord.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblWord.Name = "lblWord"
        Me.lblWord.Size = New System.Drawing.Size(317, 64)
        Me.lblWord.TabIndex = 3
        Me.lblWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblIncorrect
        '
        Me.lblIncorrect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIncorrect.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncorrect.Location = New System.Drawing.Point(359, 50)
        Me.lblIncorrect.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblIncorrect.Name = "lblIncorrect"
        Me.lblIncorrect.Size = New System.Drawing.Size(254, 64)
        Me.lblIncorrect.TabIndex = 5
        Me.lblIncorrect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel1.Controls.Add(Me.picLeftArm)
        Me.Panel1.Controls.Add(Me.picHead)
        Me.Panel1.Controls.Add(Me.picTop)
        Me.Panel1.Controls.Add(Me.picBottom)
        Me.Panel1.Controls.Add(Me.picRightArm)
        Me.Panel1.Controls.Add(Me.picLeftLeg)
        Me.Panel1.Controls.Add(Me.picRightLeg)
        Me.Panel1.Controls.Add(Me.picRope)
        Me.Panel1.Controls.Add(Me.picPost)
        Me.Panel1.Controls.Add(Me.picBody)
        Me.Panel1.Location = New System.Drawing.Point(20, 148)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(214, 148)
        Me.Panel1.TabIndex = 7
        '
        'picLeftArm
        '
        Me.picLeftArm.Image = Global.Hangman_Project.My.Resources.Resources.LeftArm
        Me.picLeftArm.Location = New System.Drawing.Point(85, 57)
        Me.picLeftArm.Margin = New System.Windows.Forms.Padding(2)
        Me.picLeftArm.Name = "picLeftArm"
        Me.picLeftArm.Size = New System.Drawing.Size(14, 11)
        Me.picLeftArm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picLeftArm.TabIndex = 16
        Me.picLeftArm.TabStop = False
        Me.picLeftArm.Visible = False
        '
        'picHead
        '
        Me.picHead.Image = Global.Hangman_Project.My.Resources.Resources.Head
        Me.picHead.Location = New System.Drawing.Point(69, 30)
        Me.picHead.Margin = New System.Windows.Forms.Padding(2)
        Me.picHead.Name = "picHead"
        Me.picHead.Size = New System.Drawing.Size(34, 29)
        Me.picHead.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picHead.TabIndex = 16
        Me.picHead.TabStop = False
        Me.picHead.Visible = False
        '
        'picTop
        '
        Me.picTop.Image = Global.Hangman_Project.My.Resources.Resources.Top
        Me.picTop.Location = New System.Drawing.Point(27, 16)
        Me.picTop.Margin = New System.Windows.Forms.Padding(2)
        Me.picTop.Name = "picTop"
        Me.picTop.Size = New System.Drawing.Size(77, 3)
        Me.picTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picTop.TabIndex = 14
        Me.picTop.TabStop = False
        Me.picTop.Visible = False
        '
        'picBottom
        '
        Me.picBottom.Image = Global.Hangman_Project.My.Resources.Resources.Bottom
        Me.picBottom.Location = New System.Drawing.Point(12, 136)
        Me.picBottom.Margin = New System.Windows.Forms.Padding(2)
        Me.picBottom.Name = "picBottom"
        Me.picBottom.Size = New System.Drawing.Size(44, 4)
        Me.picBottom.TabIndex = 12
        Me.picBottom.TabStop = False
        Me.picBottom.Visible = False
        '
        'picRightArm
        '
        Me.picRightArm.Image = Global.Hangman_Project.My.Resources.Resources.RightArm
        Me.picRightArm.Location = New System.Drawing.Point(70, 57)
        Me.picRightArm.Margin = New System.Windows.Forms.Padding(2)
        Me.picRightArm.Name = "picRightArm"
        Me.picRightArm.Size = New System.Drawing.Size(11, 12)
        Me.picRightArm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picRightArm.TabIndex = 15
        Me.picRightArm.TabStop = False
        Me.picRightArm.Visible = False
        '
        'picLeftLeg
        '
        Me.picLeftLeg.Image = Global.Hangman_Project.My.Resources.Resources.LeftLeg
        Me.picLeftLeg.Location = New System.Drawing.Point(82, 74)
        Me.picLeftLeg.Margin = New System.Windows.Forms.Padding(2)
        Me.picLeftLeg.Name = "picLeftLeg"
        Me.picLeftLeg.Size = New System.Drawing.Size(14, 12)
        Me.picLeftLeg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picLeftLeg.TabIndex = 18
        Me.picLeftLeg.TabStop = False
        Me.picLeftLeg.Visible = False
        '
        'picRightLeg
        '
        Me.picRightLeg.Image = Global.Hangman_Project.My.Resources.Resources.RightLeg
        Me.picRightLeg.Location = New System.Drawing.Point(72, 74)
        Me.picRightLeg.Margin = New System.Windows.Forms.Padding(2)
        Me.picRightLeg.Name = "picRightLeg"
        Me.picRightLeg.Size = New System.Drawing.Size(12, 14)
        Me.picRightLeg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picRightLeg.TabIndex = 17
        Me.picRightLeg.TabStop = False
        Me.picRightLeg.Visible = False
        '
        'picRope
        '
        Me.picRope.Image = Global.Hangman_Project.My.Resources.Resources.Rope
        Me.picRope.Location = New System.Drawing.Point(80, 17)
        Me.picRope.Margin = New System.Windows.Forms.Padding(2)
        Me.picRope.Name = "picRope"
        Me.picRope.Size = New System.Drawing.Size(3, 17)
        Me.picRope.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picRope.TabIndex = 15
        Me.picRope.TabStop = False
        Me.picRope.Visible = False
        '
        'picPost
        '
        Me.picPost.Image = Global.Hangman_Project.My.Resources.Resources.Post
        Me.picPost.Location = New System.Drawing.Point(27, 16)
        Me.picPost.Margin = New System.Windows.Forms.Padding(2)
        Me.picPost.Name = "picPost"
        Me.picPost.Size = New System.Drawing.Size(10, 123)
        Me.picPost.TabIndex = 13
        Me.picPost.TabStop = False
        Me.picPost.Visible = False
        '
        'picBody
        '
        Me.picBody.Image = Global.Hangman_Project.My.Resources.Resources.Body
        Me.picBody.Location = New System.Drawing.Point(81, 53)
        Me.picBody.Margin = New System.Windows.Forms.Padding(2)
        Me.picBody.Name = "picBody"
        Me.picBody.Size = New System.Drawing.Size(5, 23)
        Me.picBody.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picBody.TabIndex = 17
        Me.picBody.TabStop = False
        Me.picBody.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(637, 24)
        Me.MenuStrip1.TabIndex = 8
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNew, Me.ToolStripSeparator1, Me.mnuExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuFileNew
        '
        Me.mnuFileNew.Name = "mnuFileNew"
        Me.mnuFileNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuFileNew.Size = New System.Drawing.Size(175, 22)
        Me.mnuFileNew.Text = "&New Game"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(172, 6)
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.mnuExit.Size = New System.Drawing.Size(175, 22)
        Me.mnuExit.Text = "E&xit"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(637, 324)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblIncorrect)
        Me.Controls.Add(Me.lblWord)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Hangman Game"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.picLeftArm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picHead, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBottom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRightArm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLeftLeg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRightLeg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRope, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picPost, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBody, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblWord As System.Windows.Forms.Label
    Friend WithEvents lblIncorrect As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents picLeftArm As System.Windows.Forms.PictureBox
    Friend WithEvents picHead As System.Windows.Forms.PictureBox
    Friend WithEvents picTop As System.Windows.Forms.PictureBox
    Friend WithEvents picBottom As System.Windows.Forms.PictureBox
    Friend WithEvents picRightArm As System.Windows.Forms.PictureBox
    Friend WithEvents picLeftLeg As System.Windows.Forms.PictureBox
    Friend WithEvents picRightLeg As System.Windows.Forms.PictureBox
    Friend WithEvents picRope As System.Windows.Forms.PictureBox
    Friend WithEvents picPost As System.Windows.Forms.PictureBox
    Friend WithEvents picBody As System.Windows.Forms.PictureBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem

End Class
