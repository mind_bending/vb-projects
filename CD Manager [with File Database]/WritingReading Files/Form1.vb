﻿Option Explicit On
Option Strict On
Option Infer Off

Public Class Form1

    Dim blnFileExistFlag As Boolean
    Dim strFilePath As String
    Dim strN, strA, strP As String

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub frmMain_FormClosing(ByVal ByValsender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' save the list box information
        ' declare a StreamWriter variable
        If blnFileExistFlag Then
            Dim outFile As IO.StreamWriter

            If strFilePath = String.Empty Then

                ' open the file for output
                outFile = IO.File.CreateText("CDs.txt")

                ' write each line in the list box
                For intIndex As Integer = 0 To lstCds.Items.Count - 1
                    outFile.WriteLine(lstCds.Items(intIndex))
                Next intIndex

                ' close the file

                outFile.Close()
            Else

                ' open the file for output
                outFile = IO.File.CreateText(strFilePath & "\CDs.txt")

                ' write each line in the list box
                For intIndex As Integer = 0 To lstCds.Items.Count - 1
                    outFile.WriteLine(lstCds.Items(intIndex))
                Next intIndex

                ' close the file

                outFile.Close()

            End If

        End If

    End Sub

    Private Sub frmMain_Load(ByVal ByValsender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ' fills the list box with data
        ' stored in a sequential access file

        ' declare variables
        Dim inFile As IO.StreamReader
        Dim strInfo As String

        ' verify that the file exists
        If strFilePath = String.Empty Then
            If IO.File.Exists("CDs.txt") Then
                blnFileExistFlag = True
                ' open the file for input
                inFile = IO.File.OpenText("CDs.txt")
                ' process loop instructions until end of file
                Do Until inFile.Peek = -1
                    ' read a line from the file
                    strInfo = inFile.ReadLine
                    ' add the line to the list box
                    lstCds.Items.Add(strInfo)
                Loop

                ' close the file
                inFile.Close()
                ' select the first line in the list box
                lstCds.SelectedIndex = 0

            Else
                MessageBox.Show("Can't find the CDs.txt file",
                "CD Collection",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information)
                blnFileExistFlag = False


            End If

        Else

            If IO.File.Exists(strFilePath & "\CDs.txt") Then
                blnFileExistFlag = True
                ' open the file for input
                inFile = IO.File.OpenText(strFilePath & "\CDs.txt")
                ' process loop instructions until end of file
                Do Until inFile.Peek = -1
                    ' read a line from the file
                    strInfo = inFile.ReadLine
                    ' add the line to the list box
                    lstCds.Items.Add(strInfo)
                Loop

                ' close the file
                inFile.Close()
                ' select the first line in the list box
                lstCds.SelectedIndex = 0

            Else
                MessageBox.Show("Can't find the CDs.txt file",
                "CD Collection",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information)
                blnFileExistFlag = False



            End If
        End If

    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' adds CD information to the list box

        ' declare variables
        Dim strName As String
        Dim strArtist As String
        Dim strPrice As String
        Dim strConcatenatedInfo As String
        Dim dblPrice As Double

        ' get the CD information
        strName = InputBox("CD name:", "CD Collection")
        strArtist = InputBox("Artist:", "CD Collection")
        strPrice = InputBox("Price:", "CD Collection")

        ' format the price, then concatenate the
        ' input items, using 40 spaces for the
        ' CD name, 25 spaces for the artist name,
        ' and 5 spaces for the price

        Double.TryParse(strPrice, dblPrice)
        strPrice = dblPrice.ToString("N2")
        strConcatenatedInfo = strName.PadRight(40) &
        strArtist.PadRight(25) & strPrice.PadLeft(5)

        ' add the information to the list box
        lstCds.Items.Add(strConcatenatedInfo)

    End Sub

    Private Sub btnRemove_Click(ByVal ByValsender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        ' removes the selected line from the list box
        ' if a line is selected, remove the line
        If lstCds.SelectedIndex <> -1 Then
            lstCds.Items.RemoveAt(lstCds.SelectedIndex)
        End If
    End Sub


    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ' fills the list box with data
        ' stored in a sequential access file

        ' declare variables
        strFilePath = InputBox("Path to file:", "CD Collection")
        blnFileExistFlag = True
        Dim inFile As IO.StreamReader
        Dim strInfo As String
        lstCds.Items.Clear()
        ' verify that the file exists
        
            If IO.File.Exists(strFilePath & "\CDs.txt") Then
                blnFileExistFlag = True
                ' open the file for input
                inFile = IO.File.OpenText(strFilePath & "\CDs.txt")
                ' process loop instructions until end of file
                Do Until inFile.Peek = -1
                    ' read a line from the file
                    strInfo = inFile.ReadLine
                    ' add the line to the list box
                    lstCds.Items.Add(strInfo)
                Loop

                ' close the file
                inFile.Close()
                ' select the first line in the list box
                lstCds.SelectedIndex = 0

            Else
                MessageBox.Show("Can't find the CDs.txt file",
                "CD Collection",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information)
                blnFileExistFlag = False



        End If

    End Sub

    Private Sub lstCds_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstCds.MouseDoubleClick

        Dim strName, strArtist, strPrice, strConcatenatedInfo, strW, strN, strA, strP As String
        Dim dblPrice As Double
        strW = (lstCds.Items((lstCds.SelectedIndex))).ToString

        strN = strW.Substring(0, 40)
        strA = strW.Substring(40, 25)
        strP = strW.Substring(65, 5)
        strName = InputBox("CD name:", "CD Collection", strN)
        strArtist = InputBox("Artist:", "CD Collection", strA)
        strPrice = InputBox("Price:", "CD Collection", strP)

        ' format the price, then concatenate the
        ' input items, using 40 spaces for the
        ' CD name, 25 spaces for the artist name,
        ' and 5 spaces for the price

        Double.TryParse(strPrice, dblPrice)
        strPrice = dblPrice.ToString("N2")
        strConcatenatedInfo = strName.PadRight(40) &
        strArtist.PadRight(25) & strPrice.PadLeft(5)

        ' add the information to the list box
        lstCds.Items.Insert(lstCds.SelectedIndex, strConcatenatedInfo)
        lstCds.Items.RemoveAt(lstCds.SelectedIndex)



    End Sub


End Class
