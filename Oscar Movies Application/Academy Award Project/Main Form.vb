﻿' Name:         Academy Award Project
' Purpose:      Displays the records stored in a dataset
'               Allows the user to add records to and 
'               delete records from a dataset
' Programmer:   <your name> on <current date>

Option Explicit On
Option Strict On
Option Infer Off

Public Class frmMain

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'MoviesDataSet.tblMovies' table. You can move, or remove it, as needed.
        Me.TblMoviesTableAdapter.Fill(Me.MoviesDataSet.tblMovies)

    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' add a record to the dataset

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        ' delete a record from the dataset

    End Sub

    Private Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub txtAddYear_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAddYear.Enter
        txtAddYear.SelectAll()
    End Sub

    Private Sub txtDeleteYear_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDeleteYear.Enter
        txtDeleteYear.SelectAll()
    End Sub

    Private Sub txtAddYear_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs
                                    ) Handles txtAddYear.KeyPress, txtDeleteYear.KeyPress
        ' allows the text box to accept only numbers and the Backspace key

        If (e.KeyChar < "0" OrElse e.KeyChar > "9") AndAlso e.KeyChar <> ControlChars.Back Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCompany_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCompany.Enter
        txtCompany.SelectAll()
    End Sub

    Private Sub txtTitle_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTitle.Enter
        txtTitle.SelectAll()
    End Sub
End Class
