﻿Public Class Form1

    ' members..
    Public AddressBook As AddressBook
    Private _currentAddressIndex As Integer



    ' DataFilename—where should we store our data?
    Public ReadOnly Property DataFilename() As String
        Get
            ' get our working folder..
            Dim folder As String
            folder = Environment.CurrentDirectory
            ' return the folder with the name "Addressbook.xml"..
            Return folder & "\AddressBook.xml"
        End Get
    End Property


    ' CurrentAddressIndex—property for the current address..
    Property CurrentAddressIndex() As Integer
        Get
            Return _currentAddressIndex
        End Get
        Set(ByVal Value As Integer)
            ' set the address..
            _currentAddressIndex = Value
            ' update the display..
            PopulateFormFromAddress(CurrentAddress)
            ' set the label..
            lblAddressNumber.Text = _currentAddressIndex & " of " & AddressBook.Items.Count
        End Set
    End Property



    ' CurrentAddress—property for the current address..
    ReadOnly Property CurrentAddress() As Address
        Get
            Return AddressBook.Items(CurrentAddressIndex - 1)
        End Get
    End Property


    ' PopulateAddressFromForm—populates Address from the form fields..
    Public Sub PopulateAddressFromForm(ByVal address As Address)
        ' copy the values..
        address.FirstName = txtFirstName.Text
        address.LastName = txtLastName.Text
        address.CompanyName = txtCompanyName.Text
        address.Address1 = txtAddress1.Text
        address.Address2 = txtAddress2.Text
        address.City = txtCity.Text
        address.Region = txtRegion.Text
        address.PostalCode = txtPostalCode.Text
        address.Country = txtCountry.Text
        address.Email = txtEmail.Text
    End Sub


    Public Function AddNewAddress() As Address
        ' save the current address..
        UpdateCurrentAddress()
        ' create a new address..
        Dim newAddress As Address = AddressBook.AddAddress
        ' update the display..
        CurrentAddressIndex = AddressBook.Items.Count
        ' return the new address..
        Return newAddress
    End Function




    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' create a new address object..
        Dim address As New Address()
        ' copy the values from the form into the address..
        PopulateAddressFromForm(address)
        ' save the address..
        Dim filename As String = DataFilename
        address.Save(filename)
        ' tell the user..
        MsgBox("The address was saved to " & filename)
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click

        ' load the address using a shared method on SerializableData..
        Dim newAddress As Address = _
        SerializableData.Load(DataFilename, GetType(Address))
        ' update the display..
        PopulateFormFromAddress(newAddress)

        ' load the address book..
        AddressBook = _
        SerializableData.Load(DataFilename, GetType(AddressBook))
        ' if the address book only contains one item, add a new one..
        If AddressBook.Items.Count = 0 Then AddressBook.AddAddress()
        ' select the first item in the list..
        CurrentAddressIndex = 1

    End Sub

    ' PopulateFormFromAddress—populates the form from an
    ' address object..
    Public Sub PopulateFormFromAddress(ByVal address As Address)
        ' copy the values..
        txtFirstName.Text = address.FirstName
        txtLastName.Text = address.LastName
        txtCompanyName.Text = address.CompanyName
        txtAddress1.Text = address.Address1
        txtAddress2.Text = address.Address2
        txtCity.Text = address.City
        txtRegion.Text = address.Region
        txtPostalCode.Text = address.PostalCode
        txtCountry.Text = address.Country
        txtEmail.Text = address.Email
    End Sub


    Public Sub MoveNext()
        ' get the next index..
        Dim newIndex As Integer = CurrentAddressIndex + 1
        If newIndex > AddressBook.Items.Count Then
            newIndex = 1
        End If
        ' save any changes..
        UpdateCurrentAddress()
        ' move the record..
        CurrentAddressIndex = newIndex
    End Sub


    Public Sub MovePrevious()
        ' get the previous index..
        Dim newIndex As Integer = CurrentAddressIndex - 1
        If newIndex = 0 Then
            newIndex = AddressBook.Items.Count
        End If
        ' save changes..
        UpdateCurrentAddress()
        ' move the record..
        CurrentAddressIndex = newIndex
    End Sub



    Private Sub lnkSendEmail_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSendEmail.LinkClicked

        ' start the e-mail client..
        System.Diagnostics.Process.Start("mailto:" & txtEmail.Text)

    End Sub


    ' SaveChanges—save the address book to an XML file..
    Public Sub SaveChanges()
        ' tell the address book to save itself..
        AddressBook.Save(DataFilename)
    End Sub


    ' UpdateCurrentAddress—make sure the book has the current
    ' values currently entered into the form..
    Private Sub UpdateCurrentAddress()
        PopulateAddressFromForm(CurrentAddress)
    End Sub


    ' DeleteAddress—delete an address from the list..
    Public Sub DeleteAddress(ByVal index As Integer)
        ' delete the item from the list..
        AddressBook.Items.RemoveAt(index - 1)
        ' was that the last address?
        If AddressBook.Items.Count = 0 Then

            ' add a new address?
            AddressBook.AddAddress()
        Else
            ' make sure you have something to show..
            If index > AddressBook.Items.Count Then
                index = AddressBook.Items.Count
            End If
        End If
        ' display the record..
        CurrentAddressIndex = index
    End Sub



    Private Sub Form1_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        ' save the changes..
        UpdateCurrentAddress()
        SaveChanges()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        AddNewAddress()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        MoveNext()
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        MovePrevious()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        ' ask the user if they are ok with this?
        If MsgBox("Are you sure you want to delete this address?", _
        MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = _
        MsgBoxResult.Yes Then
            DeleteAddress(CurrentAddressIndex)
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' load the address book..
        AddressBook = _
        SerializableData.Load(DataFilename, GetType(AddressBook))
        ' if the address book only contains one item, add a new one..
        If AddressBook.Items.Count = 0 Then AddressBook.AddAddress()
        ' select the first item in the list..
        CurrentAddressIndex = 1
    End Sub
End Class
