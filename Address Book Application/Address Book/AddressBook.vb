﻿Imports System.Xml.Serialization

Public Class AddressBook
    Inherits SerializableData

    ' members..
    <XmlIgnore()> Public Items As New ArrayList()
    ' AddAddress—add a new address to the book..
    Public Function AddAddress() As Address
        ' create one..
        Dim newAddress As New Address()
        ' add it to the list..
        Items.Add(newAddress)
        ' return the address..
        Return newAddress
    End Function



    ' Addresses—property that works with the items
    ' collection as an array..
    Public Property Addresses() As Address()
        Get
            ' create a new array..
            Dim addressArray(Items.Count - 1) As Address
            Items.CopyTo(addressArray)
            Return addressArray
        End Get

        Set(ByVal Value As Address())
            ' reset the arraylist..
            Items.Clear()
            ' did you get anything?
            If Not Value Is Nothing Then
                ' go through the array and populate items..
                Dim address As Address
                For Each address In Value
                    Items.Add(address)
                Next
            End If
        End Set
    End Property




End Class
